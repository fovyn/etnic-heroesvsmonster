package be.etnic;

import be.etnic.di.DIContainer;
import be.etnic.handlers.Handler;
import be.etnic.handlers.InputHandlerFactory;

public class Main {
//    private final Handler handler;


    public static void main(String[] args) {
        DIContainer container = new DIContainer();

        container.addService(Handler.class, InputHandlerFactory.createKeyboard());
    }
}