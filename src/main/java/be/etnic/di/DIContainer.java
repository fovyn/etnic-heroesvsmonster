package be.etnic.di;

import org.reflections.Reflections;

import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DIContainer {
    private final Map<Class<?>, Object> services = new HashMap<>();

    public DIContainer() {
        Reflections reflections = new Reflections("be.etnic");
        reflections
                .getTypesAnnotatedWith(Service.class)
                .forEach(this::addService);
    }

    public <T> void addService(Class<T> service) {
        Constructor constructor = findMaxParamConstructor(service.getConstructors());

        List<Object> parameters = hydrateParameterFromConstructor(constructor);

        try {
            T instance = null;
            if (parameters.size() == 0) {
                instance = (T) constructor.newInstance();
            } else {
                instance = (T) constructor.newInstance(parameters.toArray());
            }
            T finalInstance = instance;
//            Arrays.stream(service.getInterfaces())
//                    .forEach(i -> this.services.put(i, finalInstance));
            this.services.put(service, instance);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public <TKey, T> void addService(Class<TKey> kClass, T instance) {

    }

    public <T> T get(Class<T> sClass) {
        if (!this.services.containsKey(sClass)) {
            this.addService(sClass);
        }
        return (T) this.services.get(sClass);
    }

    private List<Object> hydrateParameterFromConstructor(Constructor constructor) {
        List<Object> parameterList = new ArrayList<>();
        for (Parameter parameter : constructor.getParameters()) {
            if (!this.services.containsKey(parameter.getType())) {
                this.addService(parameter.getType());
            }
            parameterList.add(this.services.get(parameter.getType()));
        }

        return parameterList;
    }

    private Constructor findMaxParamConstructor(Constructor[] constructors) {
        Constructor useConstructor = constructors[0];

        for (int i = 1; i < constructors.length - 1; i++) {
            Constructor c1 = constructors[i];

            if (useConstructor.getParameterCount() < c1.getParameterCount()) {
                useConstructor = c1;
            }
        }

        return useConstructor;
    }
}
