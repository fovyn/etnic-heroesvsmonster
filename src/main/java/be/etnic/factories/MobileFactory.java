package be.etnic.factories;


import be.etnic.models.mobileElements.heroes.IHero;

public interface MobileFactory {
    IHero createHuman(String name);
    IHero createImpf(String name);
}
