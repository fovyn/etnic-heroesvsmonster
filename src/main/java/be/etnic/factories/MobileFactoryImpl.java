package be.etnic.factories;

import be.etnic.models.mobileElements.heroes.Human;
import be.etnic.models.mobileElements.heroes.IHero;
import be.etnic.models.mobileElements.heroes.Impf;

public class MobileFactoryImpl implements MobileFactory {
    @Override
    public IHero createHuman(String name) {
        return new Human();
    }

    @Override
    public IHero createImpf(String name) {
        return new Impf();
    }
}
