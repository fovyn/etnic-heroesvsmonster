package be.etnic.handlers;

public interface Command {
    void execute();
}
