package be.etnic.handlers;

import be.etnic.models.maps.GameMap;
import be.etnic.models.mobileElements.IMobileElement;
import be.etnic.ui.Ui;

public interface Handler {
    void initMoveCommand(GameMap map, IMobileElement element, Ui ui);

    void readMoveAction();
}
