package be.etnic.handlers;

public final class InputHandlerFactory {

    public static Handler createKeyboard() {
        return new KeyboardInputHandler();
    }
}
