package be.etnic.handlers;

import be.etnic.handlers.commands.MoveCommand;
import be.etnic.models.maps.GameMap;
import be.etnic.models.mobileElements.IMobileElement;
import be.etnic.ui.Ui;
import be.etnic.utils.Direction;

import java.util.Scanner;

class KeyboardInputHandler implements Handler {
    private Command moveNorth;
    private Command moveWest;
    private Command moveSouth;
    private Command moveEast;

    @Override
    public void initMoveCommand(GameMap map, IMobileElement element, Ui ui) {
        this.moveNorth = new MoveCommand(map, element, Direction.NORTH);
        this.moveWest = new MoveCommand(map, element, Direction.WEST);
        this.moveSouth = new MoveCommand(map, element, Direction.SOUTH);
        this.moveEast = new MoveCommand(map, element, Direction.EAST);
    }

    @Override
    public void readMoveAction() {
        Scanner scanner = new Scanner(System.in);
        String read = scanner.nextLine();

        switch (read) {
            case "z":
                this.moveNorth.execute();
                break;
            case "q":
                this.moveWest.execute();
                break;
            case "s":
                this.moveSouth.execute();
                break;
            case "d":
                this.moveEast.execute();
                break;
        }

        scanner.close();
    }
}
