package be.etnic.handlers.commands;

import be.etnic.handlers.Command;
import be.etnic.models.maps.Cell;
import be.etnic.models.maps.ContentType;
import be.etnic.models.maps.GameMap;
import be.etnic.models.maps.Pos;
import be.etnic.models.mobileElements.IMobileElement;
import be.etnic.utils.Direction;

public class MoveCommand implements Command {
    private final GameMap map;
    private final IMobileElement element;
    private final Direction direction;

    public MoveCommand(GameMap map, IMobileElement element, Direction direction) {
        this.map = map;
        this.element = element;
        this.direction = direction;
    }

    private void execute(Pos pos) {
        if (!canMove(pos)) return;

        Cell target = map.getCell(pos);
        Cell old = map.getCell(element.getPos());
        old.revertContent();
        target.changeContent(this.element.getSprite());
        this.element.setPos(pos);
    }

    @Override
    public void execute() {
        Pos pos = element.getPos();

        switch (direction) {
            case EAST:
                execute(new Pos(pos.getY(), pos.getX() - 1));
                break;
            case WEST:
                execute(new Pos(pos.getY(), pos.getX() + 1));
                break;
            case NORTH:
                execute(new Pos(pos.getY() - 1, pos.getX()));
                break;
            case SOUTH:
                execute(new Pos(pos.getY() + 1, pos.getX()));
                break;

        }
    }

    private boolean canMove(Pos pos) {
        if (!map.isInRange(pos)) return false;

        return this.map.getCell(pos).getCurrentContent() == ContentType.empty;
    }
}
