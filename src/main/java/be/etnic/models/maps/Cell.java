package be.etnic.models.maps;

public class Cell {
    private Pos pos;
    private ContentType currentContent;
    private ContentType oldContent;

    public ContentType getCurrentContent() {
        return currentContent;
    }

    public Pos getPos() {
        return this.pos;
    }

    public void changeContent(ContentType newContent) {
        this.oldContent = this.currentContent;
        this.currentContent = newContent;
    }

    public void revertContent() {
        this.currentContent = this.oldContent;
        this.oldContent = ContentType.empty;
    }

    public String toString() {
        return this.currentContent.sprite;
    }
}
