package be.etnic.models.maps;

public enum ContentType {
    hero("H"),
    monster("M"),
    empty(" ");

    public String sprite;

    ContentType(String sprite) {
        this.sprite = sprite;
    }
}
