package be.etnic.models.maps;

import be.etnic.models.mobileElements.MobileElement;
import be.etnic.models.mobileElements.heroes.IHero;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class GameMap {
    private final Map<Pos, MobileElement> monsters = new HashMap<>();
    private final int width;
    private final int height;
    private final Cell[][] cells;
    private IHero hero;

    public GameMap(int width, int height) {
        this.width = width;
        this.height = height;
        this.cells = new Cell[height][width];

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                Cell cell = new Cell();
                cell.changeContent(ContentType.empty);
                cells[y][x] = cell;
            }
        }
    }

    public Cell getCell(Pos pos) {
        return cells[pos.getY()][pos.getX()];
    }

    public boolean isInRange(Pos pos) {
        if (pos.getX() < 0 || pos.getX() >= this.width) return false;
        return pos.getY() >= 0 && pos.getY() < this.height;
    }

    public void setHero(IHero hero) {
        this.hero = hero;
        this.hero.setPos(generateRandomPosition());
        Cell cell = this.getCell(this.hero.getPos());
        cell.changeContent(ContentType.hero);
    }

    private Pos generateRandomPosition() {
        SecureRandom rnd = new SecureRandom();

        int x = rnd.nextInt(width + 1);
        int y = rnd.nextInt(height + 1);

        return new Pos(x, y);
    }

    private Stream<Cell[]> getRowStream() {
        return Arrays.stream(this.cells);
    }

    private Stream<Cell> getColumnStream(Cell[] row) {
        return Arrays.stream(row);
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int y = 0; y < height; y++) {
            builder.append("|");
            for (int x = 0; x < width; x++) {
                builder.append(cells[y][x].getCurrentContent().sprite).append("|");
            }
            builder.append("\n");
        }
        return builder.toString();
    }
}
