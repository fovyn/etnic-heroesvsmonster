package be.etnic.models.mobileElements;

import be.etnic.models.IElement;
import be.etnic.models.maps.ContentType;
import be.etnic.models.maps.Pos;

public interface IMobileElement extends IElement {
    void raiseDeadEvent();

    int getHp();

    void setHp(int hp);

    void hit(MobileElement target);

    Pos getPos();

    void setPos(Pos pos);

    ContentType getSprite();
}
