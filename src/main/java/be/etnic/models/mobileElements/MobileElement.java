package be.etnic.models.mobileElements;

import be.etnic.models.maps.ContentType;
import be.etnic.models.maps.Pos;
import be.etnic.utils.Dice;

public abstract class MobileElement implements IMobileElement {
    protected final int totalHp;
    private final int str;
    private final int sta;
    private int hp;
    private Pos pos;

    public MobileElement(int strength, int stamina) {
        this.sta = calculateStat(stamina);
        this.str = calculateStat(strength);

        this.totalHp = this.sta + modifier(this.sta);
        this.hp = this.totalHp;
    }

    public abstract ContentType getSprite();

    public boolean isAlive() {
        return this.hp > 0;
    }

    public int getHp() {
        return hp;
    }

    @Override
    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getStr() {
        return str;
    }

    public int getSta() {
        return sta;
    }

    public Pos getPos() {
        return this.pos;
    }

    public void setPos(Pos pos) {
        this.pos = pos;
    }

    @Override
    public void hit(MobileElement target) {
        int hitPoint = calculateHitPoint();

        if (target.getHp() - hitPoint <= 0) {
            target.raiseDeadEvent();
        }
        target.setHp(target.getHp() - hitPoint);
    }

    @Override
    public void raiseDeadEvent() {
    }

    private int calculateHitPoint() {
        return Dice.d4.rollDice() + modifier(this.str);
    }

    private int calculateStat(int defaultStat) {
        int rollResult = Dice.d6.rollDices(4, 3);

        int stat = rollResult + defaultStat;

        return stat + modifier(stat);
    }

    private int modifier(int stat) {
        if (stat < 5) return -1;
        if (stat < 10) return 0;
        if (stat < 15) return 1;
        return 2;
    }
}
