package be.etnic.models.mobileElements.heroes;

import be.etnic.models.maps.ContentType;
import be.etnic.models.mobileElements.MobileElement;
import be.etnic.utils.Dice;

public abstract class Hero extends MobileElement implements IHero {


    public Hero(int strength, int stamina) {
        super(strength, stamina);
    }

    @Override
    public void heal() {
        int healPoint = calculateHealPoint(this.totalHp);

        this.setHp(healPoint);
    }

    private int calculateHealPoint(int totalHp) {
        int rollResult = Dice.d10.rollDice();

        double prcHeal = 1 + rollResult / 100F;

        int totalHeal = (int) (this.totalHp * prcHeal);

        return Math.min((this.totalHp - this.getHp()), totalHeal);
    }

    @Override
    public ContentType getSprite() {
        return ContentType.hero;
    }
}
