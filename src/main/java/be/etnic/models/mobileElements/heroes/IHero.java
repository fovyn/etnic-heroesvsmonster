package be.etnic.models.mobileElements.heroes;

import be.etnic.models.mobileElements.IMobileElement;

public interface IHero extends IMobileElement {
    //    void loot(ILoot... loot);
    void heal();

}
