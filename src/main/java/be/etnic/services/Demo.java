package be.etnic.services;

import be.etnic.di.Service;

@Service
public class Demo {

    public Demo(Demo2 age) {
        System.out.println(age.age);
    }

    public void helloThere() {
        System.out.println("General Kenoby");
    }
}
