package be.etnic.utils;

import java.security.SecureRandom;
import java.util.ArrayDeque;
import java.util.Queue;

public enum Dice {
    d4(4),
    d6(6),
    d10(10);

    private final int nbFace;

    Dice(int nbFace) {
        this.nbFace = nbFace;
    }

    public int rollDices(int nbDice, int nbDiceKeep) {
        Queue<Integer> rollResults = new ArrayDeque<>();

        for (int i = 0; i < nbDice; i++) {
            rollResults.add(rollDice());
        }

        return calculateResult(rollResults, nbDiceKeep);
    }

    public int rollDice() {
        SecureRandom random = new SecureRandom();

        return random.nextInt(nbFace + 1) + 1;
    }

    private int calculateResult(Queue<Integer> rollResults, int nbDiceKeep) {
        return rollResults
                .stream()
                .unordered()
                .limit(nbDiceKeep)
                .mapToInt(i -> i)
                .sum();
    }
}
