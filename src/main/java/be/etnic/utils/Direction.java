package be.etnic.utils;

public enum Direction {
    NORTH,
    WEST,
    SOUTH,
    EAST
}
